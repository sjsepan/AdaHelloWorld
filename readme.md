# Ada Hello World

## About

    Starter sample, to illustrate compile/bind/link/execute steps in VSCode on Linux Mint

![screenshot.png](./screenshot.png?raw=true "Screenshot")

## Setup

    Install the Ada compiler for your distro; on Linux Mint (Ubuntu, Debian), type:
`sudo apt install gnat gprbuild gdb`

    Add /usr/bin/gnat to the path by adding it at the end of your .bashrc file. Note: yours will look different, depending on what was in your path and your .bashrc. file.
`export PATH="usr/local/bin:~/bin:~/.dotnet:~/.dotnet/tools:/usr/bin/gnat:~/scripts:$PATH"`

    To ensure that it is working in the current session without re-starting the session, run that resulting line in the terminal. 


## VSCode

    Find and install the "Language Support for Ada" extension by "AdaCore":
<https://marketplace.visualstudio.com/items?itemName=AdaCore.ada>

    Make sure there is an ".objs" folder in the root of your project, or VSCode will complain that there is a problem with the toolchain.

## Compile

    If you type "gnat" at the terminal, you should get a list of the gnat commands and their equivalents. For example you can type either the platform-specific program...

`x86_64-linux-gnu-gnatmake-10 -f -u -c hello_world`

    ...or you can use the alias...

`gnat compile hello_world`

## Bind, Link

    I knew from experience on Windows that there would be a Link step, but did not know about Bind. The following page guided me past the initial step of compiling, to show me how to Bind/Link:
<https://gcc.gnu.org/onlinedocs/gcc-4.6.4/gnat_ugn_unw/Running-a-Simple-Ada-Program.html>
    Although the gcc command gave the error "https://gcc.gnu.org/onlinedocs/gcc-4.6.4/gnat_ugn_unw/Running-a-Simple-Ada-Program.html", it was not necessary as the Compile was already accomplished above. Do the Bind/Link with:

`gnatbind hello_world`

`gnatlink hello_world`

## Make

    As an alternative to the Bind, Link steps, you can Make instead, as follows:

`gnatmake  hello_world`

## Run

    To run the executable, which is "hello_world" without any extension, type:
`./hello_world`

## Contact

Stephen J Sepan

<sjsepan@yahoo.com>

6-15-2023
